#include <benchmark/benchmark.h>

#include <stack.h>

#include <iostream>

Stack<int> stack;

void StressPush(benchmark::State& state) {
    if (state.thread_index == 0) {
        stack.Clear();
    }
    while (state.KeepRunning()) {
        stack.Push(0);
    }
}

void StressPushPop(benchmark::State& state) {
    if (state.thread_index == 0) {
        stack.Clear();
    }

    if (state.thread_index % 100 < 80) {
        while (state.KeepRunning()) {
            stack.Push(0);
        }
    } else {
        int value;
        while (state.KeepRunning()) {
            (void)stack.Pop(&value);
        }
    }
}

/* BENCHMARK(StressPush)->Threads(50); */
BENCHMARK(StressPushPop)->Threads(100)->UseRealTime();

BENCHMARK_MAIN();
