cmake_minimum_required(VERSION 2.8)
project(reduce)

if (TEST_SOLUTION)
    include_directories(../private/reduce)
endif()

include(../common.cmake)

if (ENABLE_PRIVATE_TESTS)
endif()

add_gtest(test_reduce test.cpp reduce.h)
add_benchmark(bench_reduce run.cpp reduce.h)
